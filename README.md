# terraform-pebmed-example
Exemplos de uso do Terraform. Foi utilizado o `backend` remoto do [Terraform.io](https://www.terraform.io/).

- [primeiros-passos](https://gitlab.com/gmontanola/terraform-pebmed-example/-/tree/main/primeiros-passos): projeto introdutório

## Observações
É possível aplicar _pipelines_ de CI/CD diretamente pelo GitLab e também modificar o `backend`
para que seja [utilizado o estado remoto do GitLab](https://docs.gitlab.com/ee/user/infrastructure/terraform_state.html).

## Instruções para o uso do `pre-commit`
É recomendado o uso do [`pre-commit`](https://pre-commit.com/) para evitar erros comuns e garantir a formatação automática
do código antes de cada _commit_.

Para instalar é possível [seguir vários caminhos](https://pre-commit.com/#install), mas a minha preferência é utilizar
o [`pipx`](https://github.com/pypa/pipx) para isolar o pacote como um programa.

### Instalação
1. Instalar o `pipx`
	1. `python3 -m pip install --user pipx` (só **python** dependendo da sua sistema operacional)
	2. `python3 -m pipx ensurepath` (só **python** dependendo do seu sistema operacional)
2. Instalar o `pre-commit`
	1. `pipx install pre-commit` (ele fica disponível no seu `PATH`)
3. Precisa de ajuda? [Docs](https://pypa.github.io/pipx/installation/).

### Ativar `pre-commit` no repositório
1. Dentro do repositório rodar: `pre-commit install`
2. Pronto, agora todos os _commits_ feitos aqui vão passar pelos `hooks` configurados.

### Rodar `pre-commit` manualmente
- `pre-commit run` (apenas arquivos que estão adicionados)
- `pre-commit run --all-files` (todos os arquivos, útil pra _lint_)
