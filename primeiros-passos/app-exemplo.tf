## S3
# Bucket
resource "aws_s3_bucket" "app-bucket" {
  bucket = "app-example-bucket-555"
}

## IAM
# Role - deve ter um documento indicando quais entidades podem impersonar a role
resource "aws_iam_role" "app-role" {
  name               = "app-exemplo-role"
  assume_role_policy = data.aws_iam_policy_document.instance-assume-role-policy.json
}

# Policy Doc - data é um objeto gerado pelo Terraform e não vira um recurso na AWS
data "aws_iam_policy_document" "instance-assume-role-policy" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

# Role Policy - define polĩticas de acesso e requer uma role alvo
resource "aws_iam_role_policy" "app-policy" {
  name = "app-s3-access"
  role = aws_iam_role.app-role.id

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "s3:GetObject",
          "s3:GetObjectVersion"
        ]
        Effect   = "Allow"
        Resource = ["${aws_s3_bucket.app-bucket.arn}/*"]
      },
    ]
  })
}

# Instance Profile - para ligar roles em EC2s
resource "aws_iam_instance_profile" "app-profile" {
  name = "app-profile"
  role = aws_iam_role.app-role.name
}

## EC2
# AMI - gera o valor da AMI mais recente do Amazon Linux 2
data "aws_ami" "amazon-linux" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-*-x86_64-gp2"] # Vai buscar essa amzn2-ami-hvm-2.0.20210721.2-x86_64-gp2
  }
}

# Instance
resource "aws_instance" "app-instance" {
  ami           = data.aws_ami.amazon-linux.id
  instance_type = "t2.micro" # Para garantir o Free Tier, use a geração 3 em produção.

  iam_instance_profile   = aws_iam_instance_profile.app-profile.id
  vpc_security_group_ids = [aws_security_group.allow-ssh.id]
  user_data              = file("scripts/bootstrap.sh")

  tags = {
    Name = "app-exemplo"
  }
}

## VPC
# Default VPC - data para pegar a ID da VPC padrão da região.
data "aws_vpc" "default" {
  default = true
}

# Security Group
resource "aws_security_group" "allow-ssh" {
  name        = "allow-ssh"
  description = "Allow SSH to the world"
  vpc_id      = data.aws_vpc.default.id

  ingress {
    description      = "SSH"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"] # Só de exemplo, não use SSH aberto para o mundão.
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}
