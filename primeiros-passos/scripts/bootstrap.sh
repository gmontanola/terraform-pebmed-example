#!/bin/bash
set -Eeuxo pipefail

# Atualiza a máquina
yum update -y

# Instala e ativa e inicia o Docker
amazon-linux-extras install docker
usermod -a -G docker ec2-user
systemctl enable --now docker

# Instala o Docker Compose e deixa ele no PATH
curl -L https://github.com/docker/compose/releases/latest/download/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

# Copia um arquivo do S3
aws s3 cp s3://app-example-bucket-555/docker-compose.yml /home/ec2-user/docker-compose.yml

# Insira o resto das coisas aqui
