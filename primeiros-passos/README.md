# Info
Exemplo bem introdutório, apenas para demonstrar uma estrutura básica e alguns conceitos iniciais.

- Sem uso de módulos, apenas recursos do próprio _provider_ da AWS
- Pouca organização nos arquivos. Tudo está dentro do `app-exemplo.tf`
- A única função do Terraform utilizada foi [`file`](https://www.terraform.io/docs/language/functions/file.html)
