terraform {
  backend "remote" {
    hostname     = "app.terraform.io"
    organization = "pebmeb-example"

    workspaces {
      name = "primeiros-passos"
    }
  }
}
